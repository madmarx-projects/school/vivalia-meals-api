const hash = require('sha1')

const hashStrings = (strings) => {
  let str = strings.join('').toLowerCase()
  return hash(str)
}

module.exports = hashStrings