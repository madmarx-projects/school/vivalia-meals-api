const mongoose = require('mongoose')

const hasSideMenuSchema = mongoose.Schema({
  date: {
    type: Date,
    required: true
  },
  qt: {
    type: Number,
    required: true
  },
  sideMenu: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'SideMenu'
  },
  pavillon: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pavillon'
  }
})

hasSideMenuSchema.index({ date: 1, pavillon: 1 }, { unique: true })

const HasSideMenu = mongoose.model('HasSideMenu', hasSideMenuSchema, 'HasSideMenus')

module.exports = HasSideMenu
