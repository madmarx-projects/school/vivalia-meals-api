const mongoose = require('mongoose')

const dietSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    description: {
        type: String,
        trim: true
    }

})

dietSchema.index({ name: 1 }, { unique: true })

const Diet = mongoose.model('Diet', dietSchema, 'Diets')


module.exports = Diet
