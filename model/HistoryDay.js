const mongoose = require('mongoose')

const historyMenuSchema = mongoose.Schema({
  dishes: [{
    type: String,
    required: true
  }],
  dietName: {
    type: String,
    required: true
  },
  nbPatient: {
    type: Number,
    required: true
  }
})

const historySideMenuSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  qt: {
    type: Number,
    required: true
  }
})

const historyDaySchema = mongoose.Schema({
  date: {
    type: Date,
    required: true,
    default: Date.now
  },
  validatedBy: {
    type: String,
    required: false
  },
  pavillon: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Pavillon'
  },
  historyMenus: [ historyMenuSchema ],
  sideMenus: [ historySideMenuSchema ]
})

historyDaySchema.index({ date: 1, pavillon: 1 }, { unique: true })

const HistoryDay = mongoose.model('HistoryDay', historyDaySchema, 'HistoryDays')

module.exports = HistoryDay
