const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    firstname: {
        type: String,
        // required: true,
        trim: true
    },
    lastname: {
        type: String,
        // required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String, // see roles in settings (settings.roles[])
        required: true,
        enum: ['NURSE', 'DIETITIAN', 'DIETITIAN_CHIEF', 'RESPONSIBLE', 'ADMIN']
    },
    pavillon: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pavillon',
        required: false
    }
})

userSchema.index({ username: 1 }, { unique: true })

const User = mongoose.model('User', userSchema, 'Users')

module.exports = User
