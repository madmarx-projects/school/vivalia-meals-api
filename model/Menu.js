const mongoose = require('mongoose')

const menuSchema = mongoose.Schema({
  dishes: [{
    type: String,
    required: true
  }],
  season: {
    type: String,
    required: true,
    default: 'ANY',
    enum: ['SUMMER', 'WINTER', 'ANY']
  },
  hash: {
    type: String,
    required: true
  }
})

menuSchema.index({ hash: 1 }, { unique: true })

const Menu = mongoose.model('Menu', menuSchema, 'Menus')

module.exports = Menu
