exports.strings = {
    langShortname: "FR",

    app: {
        title: "VivaliaMeals",
        description: "VivaliaMeals est un logiciel a destination de l'établissement hospitalier \"La Clairière\", situé à Bertrix. Il a pour vocation de remplacer le système actuel, fastidieux et sujet aux erreurs humaines."
    },

    pages: {
        home: "Accueil",
        connection: "Connexion",
        register: "S'inscrire",
        profile: "Profil",
        users: "Utilisateurs",
        diets: "Régimes",
        cycles: "Cycles",
        dishes: "Plats",
        menus: "Menus",
        pavillons: "Pavillons",
        suppMenus: "Suppléments",
        patients: "Patients"
    },

    errors: {
        notFound: {
            code: 404,
            title: "Page introuvable",
            message: "La page que vous recherchez n'est pas disponible"
        },
        internalError: {
            code: 500,
            title: "Erreur interne",
            message: "Une erreur s'est produite côté serveur"
        },
        missingParameter: {
            code: 520,
            message: "Information manquante"
        },
        duplicateKey: {
            code: 521,
            message: "Existe déjà"
        },
        objectNotFound: {
            code: 522,
            message: "Objet non trouvé"
        },
        unmatchPasswords: {
            code: 530,
            message: "Les mots de passe ne concordent pas"
        }
    },

    general: {
        firstname: "Prénom",
        lastname: "Nom de famille",
        login: "Nom d'utilisateur",
        password: "Mot de passe",
        passwordConf: "Confirmation du mot de passe",
        name: "Nom",
        description: "Description",
        language: "Langue",
        error: "Erreur",
    },

    actions: {
        signup: "Inscrire",
        signin: "Sign in",
        create: "Créer",
        actions: "Actions",
        management: "Gestion",
    },


    // PAGES
    register: {
        addUserTitle: "Ajouter un nouvel utlisateur",
    },
    connection: {
        signIn: "Se connecter",
    },
    diets: {
        listDiets: "Liste des régimes",
        noDietsTitle: "Aucun régime trouvé",
        noDescription: "Pas de description",
        newDescription: "Nouvelle description",
        newName: "Nouveau nom",
        toastTitle: "Régime",
    },
    patients: {
        listPatients: "Liste des patients",
        newFirstname: "Nouveau prénom",
        newLastname: "Nouveau nom",
        noPatientsTitle: "Aucun patient trouvé",
        associateDiet: "Régime(s) de ce patient",
        noDietWarning: "Demandez à un diététicien d'en ajouter afin de compléter le régime du patient",
        noDiet: "Pas de régime pour ce patient",
        toastTitle: "Patient",
    },


    // TOASTS
    toastMessage: {
        add: {
            success: "Bien ajouté",
            failure: "Impossible à ajouter",
        },
        update: {
            success: "Bien mis à jour",
            failure: "Impossible à mettre à jour",
        },
        delete: {
            success: "Bien supprimé",
            failure: "Impossible à supprimer",
        },
        warnings: {
            missingField: "N'oubliez pas de remplir tous les champs",
        }
    }


}