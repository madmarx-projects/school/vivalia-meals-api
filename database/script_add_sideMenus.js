/*
Database should be located in : 
	Linux : ~/dev/databases/mongodb/vivalia
	Windows : \Users\%USERNAME%\dev\databases\MongoDB\vivalia

Database name is :
	vivalia
 */



//use vivalia;

db.createCollection("SideMenus");


db.SideMenus.insert([
  {
    "name": "Pains gris"
  },
  {
    "name": "Pains blancs"
  },
  {
    "name": "Collations"
  },
  {
    "name": "Potage"
  },
]);
