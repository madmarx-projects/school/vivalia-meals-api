# VivaliaMeals Documentation

## Description

VivaliaMeals est un logiciel a destination de l'établissement hospitalier
"La Clairière", situé à Bertrix (Province de Luxembourg, Belgique).  
Son but est de faciliter la commandes des repas au sein de l'hôpital.  
Les infirmières ont la possibilité de passer des commandes pour les patients
en fonction de leurs régimes spécifiques. Les diététiciens quant à eux ont la
possiblité de créer des menus à proposer et de gérer le calendrier.

## Project

### Description

Ce projet est la partie API du système (REST). Il ne sert qu'à répondre aux
requêtes du client.  
`endpoints`: [url]/api/...  
Vers la partie [frontend](https://gitlab.com/Madmarx/vivalia-meals)

### Technos

- `express-js` pour le routing [link](http://expressjs.com/en/4x/api.html)
- `mongodb` pour la base de données [link](https://docs.mongodb.com/)
- `mongoose` comme ORM mongodb [link](https://mongoosejs.com/docs/guide.html)

### Outils conseillés

- `MongoDB Compass` pour la gestion des tables de manière graphique

### Lancer ce projet

Dépendances:

- Installer MongoDB
- Installer NodeJS

Installer les dépendances nodejs:

``` shell
npm i
```

Pour lancer le projet, a priori il suffit de lancer le script `load.sh` sous
Linux ou `load.bat` sous Windows.  
Si cela ne fonctionne pas, il vaut mieux lancer la base de données MongoDB
et le serveur NodeJS dans des **terminaux séparés**. Les commandes sont les
suivantes:

``` shell
mongod --dbpath <database_path>
npm start
```

NB: `npm start` va appeler `nodemon www.js`

## End-points

Fichiers: `routes/api/*Controller.js`

URL de base: `<domain>/api/...`

- `/auth` Gérer la connexion de l'utlisateur
- `/pavillons` CRUD les pavillons
- `/diets` CRUD des régimes
- `/meals` CRUD des repas (repas = menu à une heure et un lieu précis)
- `/menus` CRUD des menus
- `/menu-variants` CRUD des variantes des menus
- `/side-menus` CRUD des 'à-côté' (pain, soupe, etc)
- `/users` CRUD des utilisateurs
- `/history` CRUD de l'historique des commandes (not implemented)
- `/lotd` CRUD de la liste du jour (List Of The Day, partially implemented)

`CRUD` = Create, Read, Update, Delete  
`NB`: l'accès aux différents end-points est régi par un système de rôles.
Tous les utilisateurs ne peuvent pas accéder aux mêmes fonctionnalités.

## Rôles

Les rôles sont visibles dans `settings.js` => `roles`

- `NURSE` personnel infirmier, gère les patients et modifie la liste du jour
- `DIETITIAN` diététicien, gère les menus et les repas
- `DIETITIAN_CHIEF` diététicien en chef, gère les régimes et étend `DIETITIAN`
- `RESPONSIBLE` gère l'historique de commandes (not implemented)
- `ADMIN` gère tout, a accès à tous les end-points


## Settings (inutile de lire à partir d'ici)

Settings are stored in `/settings.js`. In this file you can find settings for
the servers and databases.

### Webservers

+ Server : _host, port_

### Databases

+ Database : _host, port, name, url_

You can add as many databases as you want in that file. Just access them like so

``` javascript
const databaseUrl = settings.databases.mongodb.url
```

## Tips and tricks (deprecated)

### How to fill Mongo database with a script

``` shell
mongo <your_mongo_host> <script>
```

### Page completion

In any page you have to pass several parameters

+ `title` : this will show in the tab title
+ `strings` : set of strings to use in pages

### Error handling

#### Generic error pages available

| Error code | Description |
| ---------- | ----------- |
| 404 | Page not found |
| 500 | Server internal error |

Generic error pages are stored in `/views/errors`, so use it like so

``` javascript
res.render('errors/500', {title: "titre", strings: settings.strings})
```

#### Personnalized error page

File is located at `/views/errors/error`. In order to personnalize the message you have to pass

+ `title` ex : _520_
+ `errorCode` ex : _520_
+ `errorTitle` ex : _Page not found_
+ `errorMessage` small text to explain the error

``` javascript
res.render('errors/error', {title : "500", errorCode: '500', errorTitle: 'Erreur serveur', errorMessage: "Une erreur s'est produite côté serveur."})
```

### Handlebars

#### Include partials

To include another fragment of page you should use the symbol `{{ >test }}`. It will search in `/views/partials` for the file `test.handlebars` and include its code at this place.

##### Pass parameters to a partial

Simply pass it like so
`{{ >test varTest="Value" }}`

### Language support

Languages are stored in `settings.js`

+ `settings.langs` : available languages
+ `settings.currentLang` : current language

### Password encryption

Password of users are encrypted using SHA-1 algorythm.
To do so, npm module `password-hash` is used with default parameters.

``` javascript
user.password = passwordHash.generate(user.password) // SHA-1 default
```

### Custom error codes (not ok)

| Code | Message |
| ---- | ------- |
| 520 | Missing parameter |
| 521 | Duplicate key |
| 530 | Unmatch passwords |

### Logger

#### Server side `log4js`

[log4js website](https://www.npmjs.com/package/log4js)
First import settings.js file then use it like so

``` javascript
const settings = require('./settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

logger.trace('Entering cheese testing')
logger.debug('Got cheese.')
logger.info('Cheese is Gouda.')
logger.warn('Cheese is quite smelly.')
logger.error('Cheese is too ripe!')
logger.fatal('Cheese was breeding ground for listeria.')
```

#### Client side `log4javascript_lite`

[log4javascript_lite website](http://log4javascript.org/docs/manual_lite.html#loggers)
Script is included at launch for every page in `skeleton.handlebars`

``` javascript
var logger = log4javascript.getDefaultLogger()

logger.trace('Entering cheese testing')
logger.debug('Got cheese.')
logger.info('Cheese is Gouda.')
logger.warn('Cheese is quite smelly.')
logger.error('Cheese is too ripe!')
logger.fatal('Cheese was breeding ground for listeria.')
```

### Toastr

Available toasts

``` javascript
// Server side

// ...
app.use(flash())
app.use(toastr())
app.use(function (req, res, next) {
    req.toastr.success('Message', "Title")
    res.locals.toasts = req.toastr.render()
    next()
})
// ...


// Client side

// ...
toastr.info(message, title = '', options = {})
toastr.warning(message, title = '', options = {})
toastr.error(message, title = '', options = {})
toastr.success(message, title = '', options = {})

// typically
toastr.error(strings.toastMessage.update.failure, strings.diets.toastTitle)
// ...
```

## Admin

### Password admin

- username: `admin`
- password: `admin`

Hash in database: 64be344935e89e3c9a21a31be5d2621b57d0c26b