const express = require('express'),
	router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')

Patient = require('../../model/Patient')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
	Patient.find()
		.populate('diets')
		.populate('pavillon')
		.exec((err, objects) => {
			if (err) {
				next(err)
			} else {
				res.json(objects)
			}
		})
})

// GET from pavillon
router.get('/pavillon/:id', (req, res, next) => {
	Patient.find({ pavillon: req.params.id })
		.populate('diets')
		.populate('pavillon')
		.exec((err, object) => {
			if (err) {
				next(err)
			} else {
				if (object !== null) {
					res.json(object)
				} else {
					var err = new Error()
					err.status = settings.strings.errors.objectNotFound.code
					next(err)
				}
			}
		})
})

// GET
router.get('/:id', (req, res, next) => {
	Patient.findById(req.params.id)
		.populate('diets')
		.populate('pavillon')
		.exec((err, object) => {
			if (err) {
				next(err)
			} else {
				if (object !== null) {
					res.json(object)
				} else {
					var err = new Error()
					err.status = settings.strings.errors.objectNotFound.code
					next(err)
				}
			}
		})
})

// NEW
router.put('/', VerifyToken(['ADMIN', 'NURSE']), (req, res, next) => {
	if (req.body.firstname && req.body.firstname !== '' &&
		req.body.lastname && req.body.lastname !== '' &&
		req.body.pavillon
	) {
		let data = {
			firstname: req.body.firstname.trim(),
			lastname: req.body.lastname.trim(),
			hasOrdinaryDiet: req.body.hasOrdinaryDiet === true,
			diets: req.body.diets || [],
			pavillon: req.body.pavillon
		}

		if (data.hasOrdinaryDiet) {
			delete data.diets
		}
		if (req.body.eatingPlace) {
			data.eatingPlace = req.body.eatingPlace
		}

		Patient.create(data, (err, object) => {
			if (err) {
				if (err.code === 11000) {    // duplicate key
					var err = new Error()
					err.status = settings.strings.errors.duplicateKey.code
				}
				next(err)
			} else {
				object.populate('pavillon diets', (err, result) => {
					if (err) next(err)
					res.json(result)
				})
			}
		})
	} else {
		var err = new Error()
		err.status = settings.strings.errors.missingParameter.code // 520
		next(err)
	}
})

// UPDATE
router.put('/:id', VerifyToken(['ADMIN', 'NURSE']), (req, res, next) => {
	if (req.body.firstname && req.body.firstname !== '' &&
		req.body.lastname && req.body.lastname !== ''
	) {
		let data = {
			firstname: req.body.firstname,
			lastname: req.body.lastname
		}

		if (req.body.hasOrdinaryDiet) {
			data.hasOrdinaryDiet = req.body.hasOrdinaryDiet === true
		} else {
			data.hasOrdinaryDiet = false
		}
		if (req.body.diets) {
			data.diets = req.body.diets
		}

		if (data.hasOrdinaryDiet && data.diets) {
			data.diets = []
		} else if (data.diets && data.diets.length === 0) {
			data.hasOrdinaryDiet = true
		}

		if (req.body.eatingPlace) {
			data.eatingPlace = req.body.eatingPlace
		}

		Patient.findByIdAndUpdate(req.params.id, data, { new: true })
			.populate('diets')
			.populate('pavillon')
			.exec((err, object) => {
				if (err) {
					if (err.code === 11000) {    // duplicate key
						err = new Error()
						err.status = settings.strings.errors.duplicateKey.code
					}
					next(err)
				}
				if (object !== null) {
					res.json(object)
				} else {
					var err = new Error()
					err.status = settings.strings.errors.objectNotFound.code
					next(err)
				}
			})
	} else {
		var err = new Error()
		err.status = settings.strings.errors.missingParameter.code // 520
		next(err)
	}
})

// DELETE
router.delete('/:id', VerifyToken(['ADMIN', 'NURSE']), (req, res, next) => {
	Patient.findByIdAndRemove(req.params.id, (err, object) => {
		if (err) {
			next(err)
		} else {
			if (object !== null) {
				res.json(object)
			} else {
				var err = new Error()
				err.status = settings.strings.errors.objectNotFound.code
				next(err)
			}
		}
	})
})


module.exports = router