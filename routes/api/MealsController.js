const express = require('express'),
  router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')

Meal = require('../../model/Meal')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
  Meal.find()
    .populate('menu')
    .exec((err, objects) => {
      if (err) {
        next(err)
      } else {
        res.json(objects)
      }
    })
})

// GET at date
router.get('/date/:date', function (req, res, next) {
  let date = req.params.date

  if (date !== null && date !== undefined) {
    Meal.find({ date: date })
      .populate('menu')
      .exec((err, object) => {
        if (err) {
          next(err)
        } else {
          if (object !== null) {
            res.json(object)
          } else {
            var err = new Error()
            err.status = settings.strings.errors.objectNotFound.code
            next(err)
          }
        }
      })
  } else {
    let err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

// GET
router.get('/:id', function (req, res, next) {
  Meal.findById(req.params.id)
    .populate('menu')
    .exec((err, object) => {
      if (err) {
        next(err)
      } else {
        if (object !== null) {
          res.json(object)
        } else {
          var err = new Error()
          err.status = settings.strings.errors.objectNotFound.code
          next(err)
        }
      }
    })
})

// NEW
router.put('/', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.date && 
    req.body.period && req.body.period !== '' &&
    req.body.menu
  ) {
    let data = {
      date: req.body.date || Date.now(),
      period: req.body.period,
      menu: req.body.menu
    }

    Meal.create(data, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      } else {
        object.populate('menu', (err, result) => {
          if (err) next(err)
          res.json(result)
        })
      }
    })
  } else {
    let err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})


// UPDATE
router.put('/:id', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.date || req.body.period || req.body.menu) {
    let data = { }

    if (req.body.date) {
      data.date = req.body.date
    }
    if (req.body.period) {
      data.period = req.body.period
    }
    if (req.body.menu) {
      data.menu = req.body.menu
    }

    Meal.findByIdAndUpdate(req.params.id, data, { new: true })
      .populate('menu')
      .exec((err, object) => {
        if (err) {
          if (err.code === 11000) {    // duplicate key
            var err = new Error()
            err.status = settings.strings.errors.duplicateKey.code
          }
          next(err)
        }
        if (object !== null) {
          res.json(object)
        } else {
          var err = new Error()
          err.status = settings.strings.errors.objectNotFound.code
          next(err)
        }
      })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

// DELETE
router.delete('/:id', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  Meal.findByIdAndRemove(req.params.id, (err, object) => {
    if (err) {
      next(err)
    } else {
      if (object !== null) {
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    }
  })
})


module.exports = router