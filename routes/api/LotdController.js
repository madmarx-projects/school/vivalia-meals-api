const express = require('express'),
  router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')

User = require('../../model/User')
Pavillon = require('../../model/Pavillon')
SideMenu = require('../../model/SideMenu')
Patient = require('../../model/Patient')
Meal = require('../../model/Meal')

//////////////////////////////////////////

// Make LotD
router.get('/', VerifyToken(['ADMIN', 'NURSE']), (req, res, next) => {
  let authUserId = req.body.authUserId

  if (authUserId) {
    var data = { }

    // Get user
    User.findById(authUserId, { password: 0 })
      .exec()
      .then(user => {
        data.generatedBy = user
        // Get pavillon of user
        if (user.pavillon) {
          return Pavillon.findById(user.pavillon)
        } else if (user.role === 'ADMIN') {
          // avoir les infos de tous les pavillons
          return Pavillon.find()
        } else {
          next(new Error('Il faut être ADMIN !'))
        }
      })
      .then(pav => {
        if (pav !== null) {
          if (pav instanceof Array){
            data.pavillons = pav
          } else {
            data.pavillons = [pav]
          }

          data.pavillons.forEach(p => {
            var tamer = p
            Patient.find({ pavillon: tamer._id }, { pavillon: 0 })
              .populate('diets')
              .exec()
              .then((err, objects) => {
                tamer.patients = objects
              })
          })
          // SideMenu.find()
          return Patient.find({ pavillon: pav._id }, { pavillon: 0 }).populate('diets')
        } else {
          console.log('TOUS LES PATIENTS')
          return Patient.find({}, { pavillon: 0 }).populate('diets')
        }
      })
      .then(patients => {
        //data.pavillons[0].patients = patients
      })
      .catch((err) => {
        next(err)
      })
      .then(_ => {
        res.json(data)
      })


    // if (authUser.pavillon && authUser.pavillon !== null) {
    //   // lotd d'un seul pavillon
    //   Pavillon.findById(authUser.pavillon._id, { name: 1 })
    //     .exec()
    //     .then((pavillon) => {
    //       console.log('teeeeest', pavillon)
    //     })
    // } else {
    //   // lotd de tous les pavillons
      
    // }

  } else {
    let err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }

  /// 
  // var student;

  // //Load the user
  // Student.findById(req.body.studentId).exec()

  //   //Capture student and load the course
  //   .then(function (studentFromDb) {
  //     student = studentFromDb
  //     return Course.findById(req.body.courseId).exec();
  //   })

  //   //Check if there are available seats in the course
  //   .then(function (course) {
  //     if (course.isSeatAvailable()) {
  //       //Enroll student into the course
  //       course.enrolledStudents.push(student.id);
  //       return course;
  //     } else {
  //       //throw an error
  //       throw new Error('No seats available');
  //     }
  //   })

  //   //Save the course
  //   .then(function (course) {
  //     return course.save();
  //   })

  //   //Send the response back
  //   .then(function (course) {
  //     return res.json({ message: 'Enrollment successful' })
  //   })

  //   //Catch all errors and call the error handler;
  //   .then(null, next);
    


  // User.findByIdAndUpdate(req.params._id, user, { upsert: true })
  //   .then(updatedUser => {
  //     if (addedCollections) {
  //       return User.findByIdAndUpdate(req.params._id, { $push: { _collections: { $each: addedCollections } } }, { upsert: true });
  //     }
  //   })
  //   .then(() => {
  //     return User.findById(req.params._id).populate('_collections');
  //   })
  //   .then(user => {
  //     res.json({ user });
  //   })
  //   .catch(err => {
  //     res.status(500).json({ error: err });
  //   });
})


module.exports = router