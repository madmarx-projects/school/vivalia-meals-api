const express = require('express'),
  router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')

HistoryDay = require('../../model/HistoryDay')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
  HistoryDay.find()
    .populate('pavillon')
    .exec((err, objects) => {
      if (err) {
        next(err)
      } else {
        res.json(objects)
      }
    })
})

// GET
router.get('/:id', function (req, res, next) {
  HistoryDay.findById(req.params.id)
    .populate('pavillon')
    .exec((err, object) => {
      if (err) {
        next(err)
      } else {
        if (object !== null) {
          res.json(object)
        } else {
          var err = new Error()
          err.status = settings.strings.errors.objectNotFound.code
          next(err)
        }
      }
    })
})

// NEW
router.put('/', VerifyToken(['ADMIN', 'RESPONSIBLE']), (req, res, next) => {
  if (req.body.date || Date.now() &&
    req.body.validatedBy &&
    req.body.pavillon &&
    req.body.historyMenus
  ) {
    let data = {
      date: req.body.date || Date.now(),
      validatedBy: req.body.validatedBy,
      pavillon: req.body.pavillon,
      historyMenus: req.body.historyMenus
    }

    if (req.body.sideMenus) {
      data.sideMenus = req.body.sideMenus // [ { name: String, qt: Number } ]
    }

    HistoryDay.create(data, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      } else {
        object.populate('pavillon', (err, result) => {
          if (err) next(err)
          res.json(result)
        })
      }
    })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})


// UPDATE
router.put('/:id', VerifyToken(['ADMIN', 'RESPONSIBLE']), (req, res, next) => {
  var err = new Error()
  err.status = 403 // forbidden
  next(err)
})

// DELETE
router.delete('/:id', VerifyToken(['ADMIN', 'RESPONSIBLE']), (req, res, next) => {
  HistoryDay.findByIdAndRemove(req.params.id, (err, object) => {
    if (err) {
      next(err)
    } else {
      if (object !== null) {
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    }
  })
})


module.exports = router