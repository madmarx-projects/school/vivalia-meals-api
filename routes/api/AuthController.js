const express = require('express'),
  router = express.Router()
  jwt = require('jsonwebtoken')
  hash = require('sha1')

const settings = require('../../settings')
const credentials = require('../../credentials')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

User = require('../../model/User')

router.post('/login', (req, res, next) => {
  if (req.body.username && req.body.username !== '' &&
    req.body.password && req.body.password !== ''
  ) {
    User.findOne(
      {
        username: req.body.username,
        password: hash(req.body.password + req.body.username + credentials.salt)
      },
      { password: 0 },
      (err, user) => {
        if (err) next(err)
      })
        .populate('pavillon')
        .exec((err, user) => {
          if (err) next(err)
          if (user !== null) {
            jwt.sign({ _id: user._id, role: user.role }, credentials.secret, {
              expiresIn: 10000 // expires in ... seconds
            },
            (err, token) => {
              if (err) {
                console.error(err)
                next(err)
              }
              console.log('signed token: ', token)
              res.json({
                auth: true,
                token: token,
                user: user
              })
            })
          } else {
            var err = new Error()
            err.status = settings.strings.errors.objectNotFound.code
            next(err)
          }
        })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

router.post('/logout', (req, res, next) => {
  res.status(200).send({ auth: false, token: null })
})

module.exports = router