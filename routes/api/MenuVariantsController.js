const express = require('express'),
  router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')

MenuVariant = require('../../model/MenuVariant')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
  MenuVariant.find({}, { hash: 0 })
    .populate('diet')
    .exec((err, objects) => {
      if (err) {
        next(err)
      } else {
        res.json(objects)
      }
    })
})

// GET variants of Menu
router.get('/menu/:id', function (req, res, next) {
  let id = req.params.id
  if (id !== null && id !== undefined) {
    MenuVariant.find({ menuBase: id }, { hash: 0, menuBase: 0 }, (err, object) => {
      if (err) {
        next(err)
      } else {
        if (object !== null) {
          res.json(object)
        } else {
          var err = new Error()
          err.status = settings.strings.errors.objectNotFound.code
          next(err)
        }
      }
    }).populate('diet', (err, result) => {
      if (err) next(err)
    })
  } else {
    let err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

// GET
router.get('/:id', function (req, res, next) {
  MenuVariant.findById(req.params.id, { hash: 0 })
    .populate('diet')
    .exec((err, object) => {
      if (err) {
        next(err)
      } else {
        if (object !== null) {
          res.json(object)
        } else {
          var err = new Error()
          err.status = settings.strings.errors.objectNotFound.code
          next(err)
        }
      }
    })
})


// NEW
router.put('/', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.dishes && req.body.dishes.length > 0 &&
    req.body.diet &&
    req.body.menuBase
  ) {
    let data = {
      dishes: req.body.dishes,
      diet: req.body.diet,
      menuBase: req.body.menuBase,
      hash: HashStrings(req.body.dishes)
    }

    MenuVariant.create(data, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      } else {
        object.populate('diet', (err, result) => {
          if (err) next(err)
          res.json(result)
        })
      }
    })
  } else {
    let err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})


// UPDATE
router.put('/:id', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.dishes || req.body.diet) {
    let data = {}

    if (req.body.dishes) {
      data.dishes = req.body.dishes
    }
    if (req.body.diet) {
      data.diet = req.body.diet
    }

    MenuVariant.findByIdAndUpdate(req.params.id, data, { new: true })
      .populate('diet')
      .exec((err, object) => {
        if (err) {
          if (err.code === 11000) {    // duplicate key
            var err = new Error()
            err.status = settings.strings.errors.duplicateKey.code
          }
          next(err)
        }
        if (object !== null) {
          res.json(object)
        } else {
          var err = new Error()
          err.status = settings.strings.errors.objectNotFound.code
          next(err)
        }
      })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

// DELETE
router.delete('/:id', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  MenuVariant.findByIdAndRemove(req.params.id, (err, object) => {
    if (err) {
      next(err)
    } else {
      if (object !== null) {
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    }
  })
})


module.exports = router