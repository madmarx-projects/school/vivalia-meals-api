const express = require('express'),
  router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')

Pavillon = require('../../model/Pavillon')
Patient = require('../../model/Patient')
User = require('../../model/User')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
  Pavillon.find()
    .populate('sideMenus')
    .exec((err, objects) => {
      if (err) {
        next(err)
      } else {
        res.json(objects)
      }
    })
})

// GET
router.get('/:id', (req, res, next) => {
  Pavillon.findById(req.params.id)
    .populate('sideMenus')
    .exec((err, object) => {
      if (err) {
        next(err)
      } else {
        res.json(object)
      }
    })
})

// NEW
router.put('/', VerifyToken(['ADMIN']), (req, res, next) => {
  if (req.body.name && req.body.name.trim() !== "" &&
    req.body.sideMenus
  ) {
    var data = {
      name: req.body.name.trim(),
      sideMenus: req.body.sideMenus
    }

    Pavillon.create(data, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      } else {
        object.populate('sideMenus', (err, result) => {
          if (err) next(err)
          res.json(result)
        })
      }
    })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }

})

// UPDATE
router.put('/:id', VerifyToken(['ADMIN']), (req, res, next) => {
  if (req.body.name || req.body.sideMenus) {
    var data = { }

    if (req.body.name && req.body.name != '') {
      data.name = req.body.name
    }
    if (req.body.sideMenus) {
      data.sideMenus = req.body.sideMenus
    }

    Pavillon.findByIdAndUpdate(req.params.id, data, { new: true }, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      } else {
        if (object === null) {
          var err = new Error()
          err.status = settings.strings.errors.objectNotFound.code
          next(err)
        } else {
          object.populate('sideMenus', (err, result) => {
            if (err) next(err)
            res.json(result)
          })
        }
      }
    })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

// DELETE
router.delete('/:id', VerifyToken(['ADMIN']), (req, res, next) => {
  Pavillon.findByIdAndRemove(req.params.id, (err, object) => {
    if (err) {
      next(err)
    } else {
      User.deleteMany(
        { pavillon: req.params.id },
        (err) => {
          next(err)
        })
      Patient.deleteMany(
        { pavillon: req.params.id },
        (err) => {
          next(err)
        })
      res.json(object)
    }
  })
})


module.exports = router