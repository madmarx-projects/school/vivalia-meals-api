const express = require('express'),
  router = express.Router()

const settings = require('../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('home',
    {
      title: settings.strings.pages.home,
      strings: settings.strings
    }
  )
})

module.exports = router