const express = require('express'),
  router = express.Router()

router.get('/:id', function (req, res, next) {
  res.render('profile', { title: 'Profil' })
})

module.exports = router
